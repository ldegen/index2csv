Index2Csv
=========

I have some index directories created with really old versions of Lucene (pre
3.5.0, I think).  I would like to extract the data from said indexes for
experimentation/migration.  This tool was written to support that use case. It
will iterate through the index documents and write a CSV stream to a file or to
standard out.

multi-value fields
------------------

If a field  contains more than one value for a given document, the tool will
output more than one row.  I am using a convention that aims to be compatible
with another tool_ I wrote some time ago.
The output will look something like this:

+----+----------+----------+------------------+
| id | name     | phone    | email            |
+====+==========+==========+==================+
| 1  | John Doe | +12345   | john@doe.com     |
+----+----------+----------+------------------+
|    |          | +4923456 | jdoe@job.de      |
+----+----------+----------+------------------+
|    |          |          | jd@gmail.com     |
+----+----------+----------+------------------+
| 2  | Jane Doe |          | jane@company.com |
+----+----------+----------+------------------+

This would be created by an index with two documents, where the first one has
two values for the field `phone` and three for `email`.  The second document
has no value for `phone` and one for `email`.


.. _tool: https://github.com/ldegen/agg#mapping-columns-to-attributes

Build
-----

It's a plain old maven build. Just do a ``mvn clean install``, and you should be good to go.

Usage
-----

Try ``java -jar target/index2csv-0.0.1-SNAPSHOT-jar-with-dependencies.jar``
