package index2csv;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.FieldInfos;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.ReaderUtil;

public class Cli {
  private static Options getOptions() {
    final Options options = new Options();
    options.addOption("O", "output-encoding", true, "output encoding to use (Default: UTF-8)");
    options.addOption("o", true, "write output to given file");
    options.addOption("q", false, "do not report progress");
    options.addOption("z", false, "gzip the output");
    options.addOption("H", false, "only output the header row, then exit");
    options.addOption("S", false, "skip the header row");
    options.addOption("s", true,
        "only output selected fields. Give field names separated by commas. Each field name may"
            + " optionally be followed by a colon and an alternative name to be used as the column name. \nExample:\n"
            + "-s 'foo,bar:baz,bang'");

    return options;
  }

  public static PrintStream actualOutput(CommandLine cmd) throws IOException {

    PrintStream output = System.out;
    if (cmd.hasOption('o')) {
      output = new PrintStream(cmd.getOptionValue('o'), cmd.getOptionValue('O', "utf-8"));
    }
    if (cmd.hasOption('z')) {
      return new PrintStream(new GZIPOutputStream(output));
    }
    return output;
  }

  private static String usage(final Options options) {
    final StringWriter out = new StringWriter();
    new HelpFormatter().printHelp(new PrintWriter(out), 80, "index2csv <index dir>", null, options, 2, 2, null, true);
    return out.toString().trim();
  }

  private static class ColumnMapping {
    final public String columnName;
    final public String fieldName;

    public ColumnMapping(String fieldName, String columnName) {
      this.fieldName = fieldName;
      this.columnName = columnName;
    }
  }

  public static void main(String[] args) throws IOException {

    final CommandLineParser parser = new BasicParser();
    final Options options = getOptions();
    final CommandLine cmd;
    try {
      cmd = parser.parse(options, args);
    } catch (final ParseException e) {
      System.err.println(e.getMessage());
      System.err.println(usage(options));
      System.exit(-1);
      return;
    }

    if (cmd.getArgs().length != 1) {
      System.err.println("Please give path to index directory!");
      System.err.println(usage(options));
      System.exit(-1);
    }
    if (cmd.hasOption('H') && cmd.hasOption('S')) {
      System.err.println("Options -H and -S are mutually exclusive.");
      System.err.println(usage(options));
      System.exit(-1);
    }
    Directory dir = FSDirectory.open(new File(cmd.getArgs()[0]));
    IndexReader reader = IndexReader.open(dir);
    List<ColumnMapping> mappings = getMappings(cmd, reader);

    Appendable out = actualOutput(cmd);
    CSVFormat format = CSVFormat.DEFAULT;
    CSVPrinter csvPrinter = new CSVPrinter(out, format);

    if(!cmd.hasOption('S')) {
      csvPrinter.printRecord(mappings.stream().map(m->m.columnName).collect(Collectors.toList()));
    }
    
    if(cmd.hasOption('H')) {
      System.exit(0);
    }
    boolean reportProgress = !cmd.hasOption('q');

    ConsoleProgressMonitor pm = new ConsoleProgressMonitor(new PrintWriter(System.err), 60);
    if (reportProgress) {
      pm.announceTotal(reader.maxDoc());
    }
    for (int i = 0; i < reader.maxDoc(); i++) {
      if (reader.isDeleted(i))
        continue;

      Document doc = reader.document(i);
      // for each column, values will contain a list of values.
      final List<ArrayList<String>> columns = mappings.stream()
          .map(mapping -> (new ArrayList<String>(Arrays.asList(doc.getValues(mapping.fieldName)))))
          .collect(Collectors.toCollection(() -> new ArrayList<ArrayList<String>>()));
      // for convenience, we determine the maximum number of values per column
      Stream<Integer> map = columns.stream().map(l -> l.size());
      Integer height = map.collect(Collectors.reducing(0, Math::max));

      // now we output a sequence of `height` records
      IntStream.range(0, height).forEach(n -> {
        final List<String> row = columns.stream().map(col -> (col.size() > n ? col.get(n) : null))
            .collect(Collectors.toList());
        try {
          csvPrinter.printRecord(row);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      });
      if (i % 100 == 0 && reportProgress) {
        pm.reportProgressAbsolute(i);
      }
    }
    csvPrinter.flush();
    csvPrinter.close();
    reader.close();
  }

  private static List<ColumnMapping> getMappings(CommandLine cmd, IndexReader reader) {
    FieldInfos infos = ReaderUtil.getMergedFieldInfos(reader);
    ArrayList<String> fields = new ArrayList<String>();
    infos.forEach(inf->{
      fields.add(inf.name);
    });
    if (!cmd.hasOption('s')) {
      return fields.stream().map(field -> new ColumnMapping(field, field)).collect(Collectors.toList());
    } else {
      String[] specs = cmd.getOptionValue('s').split(",");
      List<ColumnMapping> columnMappings = Arrays.stream(specs).map(spec -> {
        final String[] parts = spec.split(":");
        switch (parts.length) {
        case 1:
          return new ColumnMapping(parts[0], parts[0]);
        case 2:
          return new ColumnMapping(parts[0], parts[1]);
        default:
          throw new IllegalArgumentException("Not a valid mapping: " + spec);
        }
      })
      .collect(Collectors.toList());
      columnMappings.forEach(cm->{
        if(!fields.contains(cm.fieldName)) {
          throw new IllegalArgumentException("No such field: "+cm.fieldName);
        }
      });
      return columnMappings;
    }
  }
}
